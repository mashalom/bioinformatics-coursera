# Bioinformatics 

This project is a summary of what I learned in the [Bioinformatics for Beginners](https://www.coursera.org/learn/bioinformatics?) course offered by Coursera.

---
## Origin of Replication (In Bacterial Genome)

Before a cell divides, it must first replicate its genome. **Origin of Replication** or **_oriC_** is the region where genome replication begins. **_oriC_** is usually a short region, for example, in Vibrio Cholerar it is about 500 nucleotides long.


> How can we find the **_oriC_** in a genome?

First, let's answer the following question:
**How can a cell know to start replication in a short _oriC_ region?** There must be something special about that region!

Replication is performed by **DNA Polymerase** and the initiation of replication is done by a protein called **DnaA**. So to start replication DNA Polymerase needs the DnaA protein. DnaA binds to a short dna sequence known as **DnaA box** (~9 nucleotides long) within the replication origin region (**_oriC_**). Thus, DnaA Boxes are like "hidden messages" that tell the DnaA protein where to bind. Since DnaA needs multiple DnaA Boxes to be able to bind efficiently, there we would see several occurrences of DnaA boxes in **_oriC_**.

Good candidates for **DnaA boxes** are k-mers (most likely with mutations) and their reverse complements that occur multiple times within a short dna region.


> Algorithms for finding **Origin of Replication**: 
- By finding **clumps** of k-mers in a genome. The locations of these clumps can help identify the location of **_oriC_**.
  - `bioinformatics/origin_of_replication/approximate_pattern_matching.py`

  - For example, k-mer "**TGCA**" forms a `(25, 3)-clump` in the following genome (meaning TGCA occurs **3** times in a genome section of length **25**:  
  "c**TGCA**a**TGCA**tgacaagcc**TGCA**gttgttt"
  

- Through **Deamination**.
  - `bioinformatics/origin_of_replication/skew_array.py`
  - `bioinformatics/origin_of_replication/symbol_array.py`


---

## Motif Search

This part of the project is about finding the **Regulatory Motifs** within the upstream regions of the genes whose expression they control.

**Transcription Factor** is a protein that can bind to DNA and regulate gene expression. The function of Transcription Factor is to tell the genes to increase or decrease the gene expression. They make sure they are expressed in the right amount, at the right time, and in the right cell.

The region to which **transcription factor** binds is called **Regulatory Motif** (or Transcription Factor Binding Site).

**Regulatory Motifs** or **Transcription Factor Binding Sites** are short nucleotide sequences (usually 8-20 k-mers) located in the upstream region of genes, where transcription factors bind to in order to regulate gene expression.

> Why is it important to know where Transciption Factors are located?

Let's take a look at a transcription factor example in flies: 

**NF-kB** is a **transcription factor** responsible for activating various immunity genes in flies. Thus, if a fly is infected with a bacterium, its immunity genes will get "swithced on" by the transcription factor and fight the bacterium; and if the transcription factor bidning site of **NF-kB** is mutated then the fly might lose its ability to fight the bacterium.

> How does Transcription Factor know where to bind?

Similar to the Ori finding part of the project, the upstream regions of the genes (that transcription factors regulate) must contain some "hidden messages" to which the transciption factors bind.

If we take multiple instances of the upstream regions of a gene regulated by a Transcription Factor (let's say **NF-kB**), we will find that a 12-mer sequence similar to "TCGGGGATTTCC" (with possible mutations) occurs in each of those instances. This is a sign that the 12-mer sequence is a potential **Transcription Factor Binding Sites** of **NF-kB**.

> Algorithms for finding **Transcription Factor Binding Sites**

Our goal is to analyze multiple upstream regions of genes regulated by a given transcription factor, and find the most commonly occurring k-mers that are most similar to each other in the nucleotide sequences. Those k-mers will be refered to as "**Motifs**".

- Greedy Algorithms
  - `bioinformatics/motif_search/motif_search.py`
  - `bioinformatics/motif_search/motif_search_with_pseudocounts.py`
  - This algoritm selects the best option possible at every iteration, however it does not guarantee to produce the best overall result because it does not look at the bigger picture (just at possible options per iteration).
  - It is fast but does not gurantee to produce the most accurate solution (as opposed to Brute Force Algoritms). Instead, it quickly produces an approximation for the solution.

- Randomizes Algorithms:
  - `bioinformatics/motif_search/randomized_motif_search.py`
  - `bioinformatics/motif_search/gibbs_sampler.py`
  - Both algorithms are Monte Carlo algorithms, meaning they do not guaranteed to produce the exact solution but they quickly find an approximate solution.



---

## Usage

1. Set up and activate virtual environment by running:

```
$ python3 -m venv bioinformatics_env

$ . bioinformatics_env/bin/activate

```
Note: do this outside of the project directory.

2. Running linter (`Flake8`): 
```
$ flake8
```

3. Running autoformatter (`Black`):
```
$ black src/
$ black tests/
```

---

## Running tests

This project uses `pytest` to execute the tests and output the code coverage report.

- In the root directory run:
```
$ pytest
```
Coverage report should be added to `/coverage` directory.

  - If pytest is not able to find `bioinformatics`, make sure to install the following:

  ```    
  $ pip install -r requirements.txt
  $ pip install -e .
  ```

- To generate coverage report as an html file run: 
```
$ pytest --cov-report html:coverage --cov=src tests/
```

2. Run `python3 test_module_name.py`.
  - For example: `python3 test_count.py` will execute tests for Count module.

