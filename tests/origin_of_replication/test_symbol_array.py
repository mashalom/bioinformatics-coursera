import pytest
from bioinformatics.origin_of_replication.symbol_array import SymbolArray


def test_symbol_array():
    tests = [
        {
            "genome": "ATGCCC",
            "symbol": "C",
            "result": {0: 0, 1: 1, 2: 2, 3: 3, 4: 2, 5: 1},
        },
        {
            "genome": "CAACCGATA",
            "symbol": "C",
            "result": {0: 2, 1: 2, 2: 2, 3: 2, 4: 1, 5: 0, 6: 1, 7: 1, 8: 1},
        },
        {"genome": "CCCCC", "symbol": "C", "result": {0: 2, 1: 2, 2: 2, 3: 2, 4: 2}},
        {
            "genome": "AGTTTGA",
            "symbol": "C",
            "result": {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0},
        },
        {
            "genome": "ACGTAC",
            "symbol": "G",
            "result": {0: 1, 1: 1, 2: 1, 3: 0, 4: 0, 5: 0},
        },
    ]

    for test in tests:
        genome = test.get("genome")
        symbol = test.get("symbol")
        result = test.get("result")
        assert SymbolArray(genome, symbol) == result
