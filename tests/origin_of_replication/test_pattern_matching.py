import pytest
from bioinformatics.origin_of_replication.pattern_matching import PatternMatching


def test_pattern_matching():
    tests = [
        {"pattern": "GTT", "genome": "GTTGAGTTGTTAGT", "result": [0, 5, 8]},
        {"pattern": "ATA", "genome": "ATTTATCGTA", "result": []},
        {"pattern": "CC", "genome": "CCCCC", "result": [0, 1, 2, 3]},
        {"pattern": "CAC", "genome": "CACACGTAC", "result": [0, 2]},
    ]
    for test in tests:
        pattern = test.get("pattern")
        genome = test.get("genome")
        result = test.get("result")
        assert PatternMatching(Genome=genome, Pattern=pattern) == result
