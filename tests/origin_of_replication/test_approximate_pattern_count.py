import pytest
from bioinformatics.origin_of_replication.approximate_pattern_count import (
    ApproximatePatternCount,
)


def test_approximate_pattern_count():
    tests = [
        {"pattern": "AAAA", "text": "AGGATAAATAG", "d": 2, "result": 7},
        {"pattern": "AAAA", "text": "AGGATAAATAG", "d": 1, "result": 4},
        {"pattern": "AAAA", "text": "AGGATAAATAG", "d": 0, "result": 0},
        {"pattern": "AGT", "text": "GTAAGTGGC", "d": 0, "result": 1},
    ]

    for test in tests:
        text = test.get("text")
        pattern = test.get("pattern")
        d = test.get("d")
        result = test.get("result")
        assert ApproximatePatternCount(Text=text, Pattern=pattern, d=d) == result
