import pytest
from bioinformatics.origin_of_replication.dna_reverse_complement import (
    Complement,
    Reverse,
    ReverseComplement,
)


def test_complement():
    tests = [
        {"pattern": "AGTCGCATAGT", "result": "TCAGCGTATCA"},
        {"pattern": "AAAA", "result": "TTTT"},
        {"pattern": "TTTT", "result": "AAAA"},
        {"pattern": "GGG", "result": "CCC"},
        {"pattern": "ATTG", "result": "TAAC"},
    ]
    for test in tests:
        pattern = test.get("pattern")
        result = test.get("result")
        assert Complement(pattern) == result


def test_reverse():
    tests = [
        {"pattern": "AACGTA", "result": "ATGCAA"},
        {"pattern": "TGGT", "result": "TGGT"},
        {"pattern": "AAAAAA", "result": "AAAAAA"},
        {"pattern": "CGTGTC", "result": "CTGTGC"},
    ]

    for test in tests:
        pattern = test.get("pattern")
        result = test.get("result")
        assert Reverse(pattern) == result


def test_reverse_complement():
    tests = [
        {"pattern": "AATGCG", "result": "CGCATT"},
        {"pattern": "TAAT", "result": "ATTA"},
        {"pattern": "GTGTGT", "result": "ACACAC"},
        {"pattern": "AAA", "result": "TTT"},
    ]

    for test in tests:
        pattern = test.get("pattern")
        result = test.get("result")
        assert ReverseComplement(pattern) == result
