import pytest
from bioinformatics.origin_of_replication.most_frequent_kmer_finder import (
    FrequentWords,
    FrequencyMap,
)


def _test_runner(tests):
    for test in tests:
        text = test.get("text")
        k = test.get("k")
        result = test.get("result")
        if isinstance(result, dict):
            assert FrequencyMap(text, k) == result
        else:
            assert FrequentWords(text, k) == result


def test_frequent_words():
    tests = [
        {"text": "ACGTACGTAG", "k": 4, "result": ["ACGT", "CGTA"]},
        {"text": "", "k": 4, "result": []},
        {"text": "TACG", "k": 5, "result": []},
        {"text": "TACGA", "k": 5, "result": ["TACGA"]},
        {"text": "CGTAGG", "k": 3, "result": ["CGT", "GTA", "TAG", "AGG"]},
    ]
    _test_runner(tests)


def test_frequency_map():
    tests = [
        {
            "text": "ACGTACGTAG",
            "k": 4,
            "result": {"ACGT": 2, "CGTA": 2, "GTAC": 1, "TACG": 1, "GTAG": 1},
        },
        {"text": "GGGGGG", "k": 3, "result": {"GGG": 4}},
        {"text": "TGAGTA", "k": 3, "result": {"TGA": 1, "GAG": 1, "AGT": 1, "GTA": 1}},
        {"text": "CACA", "k": 2, "result": {"CA": 2, "AC": 1}},
        {"text": "", "k": 2, "result": {}},
    ]
    _test_runner(tests)
