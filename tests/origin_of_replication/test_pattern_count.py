import pytest
from bioinformatics.origin_of_replication.pattern_count import PatternCount


def test_pattern_count():
    tests = [
        {"text": "AACACACGTG", "pattern": "CAC", "result": 2},
        {"text": "GTTGTCGTTAC", "pattern": "GTTA", "result": 1},
        {"text": "CTAGTCAGG", "pattern": "GGA", "result": 0},
        {"text": "AATGC", "pattern": "AATGC", "result": 1},
    ]

    for test in tests:
        text = test.get("text")
        pattern = test.get("pattern")
        result = test.get("result")
        assert PatternCount(text, pattern) == result
