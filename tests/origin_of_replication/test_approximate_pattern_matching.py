import pytest
from bioinformatics.origin_of_replication.approximate_pattern_matching import (
    ApproximatePatternMatching,
)


def test_approximate_pattern_matching():
    tests = [
        {"text": "ACGTAGGTA", "pattern": "CGTA", "d": 1, "result": [1, 5]},
        {"text": "ACACACA", "pattern": "ATT", "d": 2, "result": [0, 2, 4]},
        {"text": "AGTGGTAGGTGGT", "pattern": "GGTG", "d": 0, "result": [7]},
        {"text": "AGTGGTAGGTGGT", "pattern": "TTT", "d": 1, "result": []},
    ]

    for test in tests:
        text = test.get("text")
        pattern = test.get("pattern")
        d = test.get("d")
        result = test.get("result")
        assert ApproximatePatternMatching(Text=text, Pattern=pattern, d=d) == result
