import pytest
from bioinformatics.origin_of_replication.hamming_distance import HammingDistance


def test_hamming_distance():
    tests = [
        {"p": "GGTAGTAG", "q": "GGTAGTAG", "result": 0},
        {"p": "GGTAGTAG", "q": "ACGTAGTA", "result": 8},
        {"p": "ATGTAT", "q": "AGGCAT", "result": 2},
        {"p": "", "q": "", "result": 0},
    ]

    for test in tests:
        p = test.get("p")
        q = test.get("q")
        result = test.get("result")
        assert HammingDistance(p, q) == result
