import pytest
from bioinformatics.origin_of_replication.skew_array import SkewArray, MinimumSkew


def test_skew_array():
    tests = [
        {
            "genome": "AACCATCGAAGCTTAGGGTAGGC",
            "result": [
                0,
                0,
                0,
                -1,
                -2,
                -2,
                -2,
                -3,
                -2,
                -2,
                -2,
                -1,
                -2,
                -2,
                -2,
                -2,
                -1,
                0,
                1,
                1,
                1,
                2,
                3,
                2,
            ],
            "oriLocationIndex": 7,
            "terLocationIndex": 22,
        },
        {
            "genome": "CCCTGGGG",
            "result": [0, -1, -2, -3, -3, -2, -1, 0, 1],
            "oriLocationIndex": 3,
            "terLocationIndex": 8,
        },
        {
            "genome": "CCACCA",
            "result": [0, -1, -2, -2, -3, -4, -4],
            "oriLocationIndex": 5,
            "terLocationIndex": 0,
        },
        {
            "genome": "GGTGG",
            "result": [0, 1, 2, 2, 3, 4],
            "oriLocationIndex": 0,
            "terLocationIndex": 5,
        },
        {
            "genome": "AATTAT",
            "result": [0, 0, 0, 0, 0, 0, 0],
            "oriLocationIndex": 0,
            "terLocationIndex": 0,
        },
    ]

    for test in tests:
        genome = test.get("genome")
        result = test.get("result")
        oriLocationIndex = test.get("oriLocationIndex")
        terLocationIndex = test.get("terLocationIndex")

        skewArray = SkewArray(genome)
        minSkew = min(skewArray)
        maxSkew = max(skewArray)
        assert skewArray == result
        assert skewArray.index(minSkew) == oriLocationIndex
        assert skewArray.index(maxSkew) == terLocationIndex


def test_minimum_skew():
    tests = [
        {"genome": "CCAGTAGG", "result": [2, 3]},
        {"genome": "CCCC", "result": [4]},
        {"genome": "GTTAGCG", "result": [0]},
        {"genome": "TGACCCGTC", "result": [6, 9]},
        {"genome": "GTTGAGAG", "result": [0]},
    ]

    for test in tests:
        genome = test.get("genome")
        result = test.get("result")
        assert MinimumSkew(genome) == result
