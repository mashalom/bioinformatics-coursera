import pytest
from bioinformatics.motif_search.gibbs_sampler import (
    Normalize,
    WeightedDie,
    ProfileGeneratedString,
    GibbsSampler,
)


def test_gibbs_sampler():
    tests = [
        {
            "dna": ["AAGGGGAAA", "GGGGCCCCC", "TTTGGGGTT"],
            "k": 3,
            "N": 100,
            "result": ["GGG", "GGG", "GGG"],
        },
        {
            "dna": ["ACCT", "CCTA", "CCAA", "ACCT"],
            "k": 3,
            "N": 50,
            "result": ["CCT", "CCT", "CCA", "CCT"],
        },
        {
            "dna": ["AAAAAAAAATTTTAAAAA", "TTTTTTTTTTTTTTCTTT", "GGTTTTGGGGGGGGGGGG"],
            "k": 4,
            "N": 200,
            "result": ["TTTT", "TTTT", "TTTT"],
        },
    ]

    for test in tests:
        dna = test.get("dna")
        k = test.get("k")
        N = test.get("N")
        result = test.get("result")
        assert GibbsSampler(dna, k, N) == result


def test_profile_generated_string():
    tests = [
        {
            "text": "ACGAAT",
            "profile": {"A": [0, 0], "C": [1, 0], "G": [0, 1], "T": [0, 0]},
            "k": 2,
            "result": "CG",
        },
        {
            "text": "ACGAAT",
            "profile": {"A": [0, 1, 1], "C": [0, 0, 0], "G": [1, 0, 0], "T": [0, 0, 0]},
            "k": 3,
            "result": "GAA",
        },
    ]

    for test in tests:
        text = test.get("text")
        profile = test.get("profile")
        k = test.get("k")
        result = test.get("result")
        assert ProfileGeneratedString(text, profile, k) == result

    ## Testing individual case
    text = "AACTT"
    profile = {
        "A": [0.05, 0.05, 0.1],
        "C": [0.75, 0.05, 0.05],
        "G": [0.01, 0.05, 0.05],
        "T": [0.1, 0.85, 0.8],
    }
    k = 3
    resultingRandomKmer = "CTT"
    timesTested = 15
    timesSelected = 0

    for i in range(timesTested):
        if ProfileGeneratedString(text, profile, k) == resultingRandomKmer:
            timesSelected += 1

    assert timesSelected > timesSelected / 2


def test_normalize():
    tests = [
        {
            "probabilities": {"A": 0.2, "C": 0.8, "G": 0.4, "T": 0.6},
            "result": {"A": 0.1, "C": 0.4, "G": 0.2, "T": 0.3},
        },
        {
            "probabilities": {"A": 0.3, "C": 0.2, "G": 0.4, "T": 0.1},
            "result": {"A": 0.3, "C": 0.2, "G": 0.4, "T": 0.1},
        },
        {
            "probabilities": {"A": 0.1, "C": 0.1, "G": 0.1, "T": 0.1},
            "result": {"A": 0.25, "C": 0.25, "G": 0.25, "T": 0.25},
        },
    ]

    for test in tests:
        probabilities = test.get("probabilities")
        result = test.get("result")
        assert Normalize(probabilities) == result


def test_weighted_die():
    tests = [
        {"probabilities": {"A": 1, "C": 0, "G": 0, "T": 0}, "result": "A"},
        {"probabilities": {"A": 0, "C": 0, "G": 1, "T": 0}, "result": "G"},
    ]
    probabilitiesTest = {"A": 0.1, "C": 0.05, "G": 0.05, "T": 0.8}
    timesSelected = 0
    timesTested = 20

    for test in tests:
        probabilities = test.get("probabilities")
        result = test.get("result")
        assert WeightedDie(probabilities) == result

    for i in range(timesTested):
        if WeightedDie(probabilitiesTest) == "T":
            timesSelected += 1

    assert timesSelected > timesTested / 2
