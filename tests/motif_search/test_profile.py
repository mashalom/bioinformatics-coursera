import pytest
from bioinformatics.motif_search.profile import Profile, ProfileWithPseudocounts


def test_profile():
    tests = [
        {
            "motifs": ["GTA", "CAT", "TTA", "ATG"],
            "result": {
                "A": [0.25, 0.25, 0.5],
                "C": [0.25, 0, 0],
                "G": [0.25, 0, 0.25],
                "T": [0.25, 0.75, 0.25],
            },
        },
        {
            "motifs": ["TTA", "TAT", "TTA", "TTG"],
            "result": {
                "A": [0, 0.25, 0.5],
                "C": [0, 0, 0],
                "G": [0, 0, 0.25],
                "T": [1, 0.75, 0.25],
            },
        },
        {
            "motifs": ["GGAC", "GGAC", "CCAG", "TAGC", "GAAC"],
            "result": {
                "A": [0, 0.4, 0.8, 0],
                "C": [0.2, 0.2, 0, 0.8],
                "G": [0.6, 0.4, 0.2, 0.2],
                "T": [0.2, 0, 0, 0],
            },
        },
    ]

    for test in tests:
        motifs = test.get("motifs")
        result = test.get("result")
        assert Profile(motifs) == result


def test_profile_with_pseudocounts():
    tests = [
        {
            "motifs": ["GTA", "CAT", "TTA", "ATG"],
            "result": {
                "A": [0.25, 0.25, 0.375],
                "C": [0.25, 0.125, 0.125],
                "G": [0.25, 0.125, 0.25],
                "T": [0.25, 0.5, 0.25],
            },
        },
        {
            "motifs": ["TTA", "TAT", "TTA", "TTG"],
            "result": {
                "A": [0.125, 0.25, 0.375],
                "C": [0.125, 0.125, 0.125],
                "G": [0.125, 0.125, 0.25],
                "T": [0.625, 0.5, 0.25],
            },
        },
    ]

    for test in tests:
        motifs = test.get("motifs")
        result = test.get("result")
        assert ProfileWithPseudocounts(motifs) == result
