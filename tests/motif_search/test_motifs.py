import pytest
from bioinformatics.motif_search.motifs import Motifs, RandomMotifs


def test_motifs():
    tests = [
        {
            "dna": ["TTAC", "TACT", "TGAC", "TAGT"],
            "profile": {
                "A": [0.2, 0.8, 0.1],
                "C": [0, 0.1, 0.3],
                "G": [0.3, 0.1, 0.3],
                "T": [0.5, 0, 0.3],
            },
            "result": ["TAC", "TAC", "GAC", "TAG"],
        },
        {
            "dna": ["TGAAGTC", "CACTAGA"],
            "profile": {
                "A": [0.5, 0.2, 0, 0.1],
                "C": [0.3, 0.2, 0.4, 0],
                "G": [0.1, 0.3, 0.6, 0.1],
                "T": [0.1, 0.3, 0, 0.8],
            },
            "result": ["AAGT", "CACT"],
        },
        {
            "dna": ["TGAAGTC", "CACTAGA"],
            "profile": {
                "A": [1, 1, 1, 1],
                "C": [0, 0, 0, 0],
                "G": [0, 0, 0, 0],
                "T": [0, 0, 0, 0],
            },
            "result": ["TGAA", "CACT"],
        },
    ]

    for test in tests:
        dna = test.get("dna")
        profile = test.get("profile")
        result = test.get("result")
        assert Motifs(profile, dna) == result


def test_random_motifs():
    tests = [
        {
            "dna": ["AATGGTA", "TTACCGA", "GGGCATC", "TACCGTA"],
            "k": 3,
            "numOfSelectedKmers": 4,
        },
        {
            "dna": ["AAAAA", "GGGGG", "CCCCC", "TTTTT"],
            "k": 3,
            "numOfSelectedKmers": 4,
            "result": ["AAA", "GGG", "CCC", "TTT"],
        },
    ]

    for test in tests:
        dna = test.get("dna")
        k = test.get("k")
        numOfSelectedKmers = test.get("numOfSelectedKmers")
        result = test.get("result")
        randomMotifs = RandomMotifs(dna, k)

        assert len(randomMotifs) == numOfSelectedKmers
        if result:
            assert randomMotifs == result
