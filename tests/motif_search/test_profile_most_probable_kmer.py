import pytest
from bioinformatics.motif_search.profile_most_probable_kmer import (
    ProfileMostProbableKmer,
)


def test_profile_most_probable_kmer():
    tests = [
        {
            "text": "AACATGTACTAA",
            "k": 3,
            "profile": {
                "A": [0.2, 0.1, 0],
                "C": [0.1, 0.2, 0.3],
                "G": [0.2, 0.4, 0.1],
                "T": [0.5, 0.3, 0.6],
            },
            "result": "TGT",
        },
        {
            "text": "ACCTTTTAGGTAATAATATAGCCCGAGGCT",
            "k": 5,
            "profile": {
                "A": [0, 1, 1, 0, 1],
                "C": [0, 0, 0, 0, 0],
                "G": [0, 0, 0, 0, 0],
                "T": [1, 0, 0, 1, 0],
            },
            "result": "TAATA",
        },
        {
            "text": "CCTAGGACT",
            "k": 4,
            "profile": {
                "A": [0.25, 0.25, 0.25, 0.24],
                "C": [0.25, 0.25, 0.25, 0.24],
                "G": [0.25, 0.25, 0.25, 0.24],
                "T": [0.25, 0.25, 0.25, 0.24],
            },
            "result": "CCTA",
        },
    ]
    for test in tests:
        text = test.get("text")
        k = test.get("k")
        profile = test.get("profile")
        result = test.get("result")
        assert ProfileMostProbableKmer(text, k, profile) == result
