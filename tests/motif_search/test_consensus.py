import pytest
from bioinformatics.motif_search.consensus import Consensus


def test_consensus():
    tests = [
        {"consensus": "CAT", "motifs": ["AAT", "CAA", "CTT"]},
        {"consensus": "AAA", "motifs": ["AAA", "CCC", "TTT"]},
        {"consensus": "GGCA", "motifs": ["GCCA", "AGCA", "GGCC", "GTTC"]},
    ]

    for test in tests:
        consensus = test.get("consensus")
        motifs = test.get("motifs")
        assert Consensus(motifs) == consensus
