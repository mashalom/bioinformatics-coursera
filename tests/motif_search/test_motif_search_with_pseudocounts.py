import pytest
from bioinformatics.motif_search.motif_search_with_pseudocounts import (
    GreedyMotifSearchWithPseudocounts,
)


def test_motif_search_with_pseudocounts():
    tests = [
        {"dna": ["AACGC", "ACCGT", "CCGGA"], "k": 3, "result": ["ACG", "ACC", "CCG"]},
        {
            "dna": ["AAGCCAAA", "AATCCTGG", "CGTACTTG", "ATGTTTTG"],
            "k": 3,
            "result": ["AAG", "AAT", "ACT", "ATG"],
        },
        {
            "dna": ["TTGCAGT", "TAAGCTG", "GGTGAAC"],
            "k": 4,
            "result": ["GCAG", "GCTG", "GGTG"],
        },
    ]

    for test in tests:
        dna = test.get("dna")
        k = test.get("k")
        result = test.get("result")
        assert GreedyMotifSearchWithPseudocounts(dna, k) == result
