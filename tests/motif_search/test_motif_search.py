import pytest
from bioinformatics.motif_search.motif_search import MotifSearch


def test_motif_search():
    tests = [
        {
            "dna": ["GGCGTT", "AAGAAT", "CACGTC"],
            "k": 3,
            "result": ["GCG", "AAG", "ACG"],
        },
        {
            "dna": ["AAATTTTAAA", "TTTTGGGGGG", "CCCCTTTTCC", "AGTTTTTTTT"],
            "k": 4,
            "result": ["TTTT", "TTTT", "TTTT", "TTTT"],
        },
        {
            "dna": ["AGTAA", "GTTGG", "GTACC", "CCGTA"],
            "k": 3,
            "result": ["GTA", "GTT", "GTA", "GTA"],
        },
    ]

    for test in tests:
        dna = test.get("dna")
        k = test.get("k")
        result = test.get("result")
        assert MotifSearch(dna, k) == result
