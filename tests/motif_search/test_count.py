import pytest
from bioinformatics.motif_search.count import Count, CountWithPseudocounts

motifs = [["ACCT", "ATTT"], ["GG"], ["ACGT", "ACGT", "ACGT"]]


def test_count_function():
    tests = [
        {
            "motifs": motifs[0],
            "result": {
                "A": [2, 0, 0, 0],
                "C": [0, 1, 1, 0],
                "G": [0, 0, 0, 0],
                "T": [0, 1, 1, 2],
            },
        },
        {
            "motifs": motifs[1],
            "result": {"A": [0, 0], "C": [0, 0], "G": [1, 1], "T": [0, 0]},
        },
        {
            "motifs": motifs[2],
            "result": {
                "A": [3, 0, 0, 0],
                "C": [0, 3, 0, 0],
                "G": [0, 0, 3, 0],
                "T": [0, 0, 0, 3],
            },
        },
    ]
    for test in tests:
        assert Count(test.get("motifs")) == test.get("result")


def test_count_with_pseudocounts():
    tests = [
        {
            "motifs": motifs[0],
            "result": {
                "A": [3, 1, 1, 1],
                "C": [1, 2, 2, 1],
                "G": [1, 1, 1, 1],
                "T": [1, 2, 2, 3],
            },
        },
        {
            "motifs": motifs[1],
            "result": {"A": [1, 1], "C": [1, 1], "G": [2, 2], "T": [1, 1]},
        },
        {
            "motifs": motifs[2],
            "result": {
                "A": [4, 1, 1, 1],
                "C": [1, 4, 1, 1],
                "G": [1, 1, 4, 1],
                "T": [1, 1, 1, 4],
            },
        },
    ]
    for test in tests:
        assert CountWithPseudocounts(test.get("motifs")) == test.get("result")
