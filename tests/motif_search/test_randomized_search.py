import pytest
from bioinformatics.motif_search.randomized_motif_search import RandomizedMotifSearch


def test_randomized_motif_search():
    tests = [
        {
            "dna": ["CACGAC", "AGGATT", "TAACGA"],
            "k": 4,
            "result": ["ACGA", "AGGA", "ACGA"],
        },
        {
            "dna": ["TTACAC", "GATTAG", "CCGTCA", "GTTAGA"],
            "k": 3,
            "result": ["TTA", "TTA", "TCA", "TTA"],
        },
    ]
    for test in tests:
        dna = test.get("dna")
        k = test.get("k")
        result = test.get("result")
        assert RandomizedMotifSearch(dna, k) == result
