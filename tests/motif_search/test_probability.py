import pytest
from bioinformatics.motif_search.probability import Pr


def test_probability():
    tests = [
        {
            "text": "AACC",
            "profile": {
                "A": [1, 1, 0, 0],
                "C": [0, 0, 1, 1],
                "T": [0, 0, 0, 0],
                "G": [0, 0, 0, 0],
            },
            "result": 1.0,
        },
        {
            "text": "ACT",
            "profile": {
                "A": [0.2, 0.2, 0.2],
                "C": [0.5, 0.5, 0],
                "T": [0, 0.3, 0.2],
                "G": [0.3, 0, 0.6],
            },
            "result": 0.02,
        },
        {
            "text": "TAT",
            "profile": {
                "A": [0.2, 0.2, 0.2],
                "C": [0.5, 0.5, 0],
                "T": [0, 0.3, 0.2],
                "G": [0.3, 0, 0.6],
            },
            "result": 0.0,
        },
    ]
    for test in tests:
        text = test.get("text")
        profile = test.get("profile")
        result = test.get("result")
        assert Pr(text, profile) == result
