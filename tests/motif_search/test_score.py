import pytest
from bioinformatics.motif_search.score import Score


def test_score():
    tests = [
        {"motifs": ["AACGT", "ATCGT", "AACCT"], "result": 2},
        {"motifs": ["GTCC", "GTCC", "GTCC", "GTCC"], "result": 0},
        {"motifs": ["GTCA", "TATG", "ACAC"], "result": 8},
    ]

    for test in tests:
        assert Score(test.get("motifs")) == test.get("result")
