# ReverseComplement returns the complementary DNA strand given a DNA strand.
#
# For example: Given a DNA string in 5 --> 3 direction, this function will return
# its complement also in 5 --> 3 direction.
# Input:
# Pattern = 'AGTAC'
# Output: 'GTACT'


def ReverseComplement(Pattern):
    return Complement(Reverse(Pattern))


# Reverse function takes a string Pattern and outputs the reverse of that string.
#
# For example:
# Input:
# Pattern = "ACGT"
# Output: "TGCA"


def Reverse(Pattern):
    result = ""
    for char in Pattern:
        result = char + result
    return result


# Complement function takes a string Pattern representing a DNA strand in 5 --> 3
# direction and outputs its complementary strand in 3 --> 5 direction.
#
# For example:
# Input:
# Pattern = "AGTCGCATAGT"
# Output: "TCAGCGTATCA"


def Complement(Pattern):
    complements = {"A": "T", "T": "A", "C": "G", "G": "C"}
    result = ""
    for char in Pattern:
        result = result + complements[char]
    return result
