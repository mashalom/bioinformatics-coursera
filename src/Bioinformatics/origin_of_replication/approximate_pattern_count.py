from bioinformatics.origin_of_replication.hamming_distance import HammingDistance

# Similarly to ApproximatePatternMatching this function looks for k-mer Pattern
# with up to D number of mutations in the DNA string Text and counts the number
# of its occurences.

# For example,
# Input:
# Pattern = "AAAA"
# Text = "AGGATAAATAG"
# d = 2
# Output: 7


def ApproximatePatternCount(Pattern, Text, d):
    count = 0
    for i in range(len(Text) - len(Pattern) + 1):
        if HammingDistance(Pattern, Text[i : i + len(Pattern)]) <= d:
            count = count + 1
    return count
