from bioinformatics.origin_of_replication.hamming_distance import HammingDistance

# This function helps identify potential DnaA boxes by finding how many times k-mer
# Pattern with up to D number of mutations appears within a DNA region Text.
# Statistically if we find a k-mer in a DNA string repeating several times, it's a good
# sign that it could be the "secret message" or the DnaA box.
# The DNA region that we are examining here is not just a random DNA region, instead
# the same region from the SkewArray or SymbolArray functions is used (since that
# region points to the approximate location of the Ori). Here our goal is to find the
# nucleotide sequence where DNA replication starts.

# Note: DnaA boxes are k-mers where DNA replication starts.

# For example,
# Input:
# Text = "ACGTAGGTA"
# Pattern = "CGTA"
# D = 1
# Output: [1, 5]


def ApproximatePatternMatching(Text, Pattern, d):
    positions = []
    for i in range(len(Text) - len(Pattern) + 1):
        if HammingDistance(Pattern, Text[i : i + len(Pattern)]) <= d:
            positions.append(i)
    return positions
