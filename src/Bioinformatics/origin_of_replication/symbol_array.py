from bioinformatics.origin_of_replication.pattern_count import PatternCount

# Finding Origin of Replication through Deamination.

# This function can be used to construct a Symbol Array that can give us a hint on
# where the (ori) origin of replication is given a Genome. This function assumes that
# the provided Genome is a circular genome such as most of bacterial genomes.

# Replication of Reverse (Leading) Half Strand proceeds quickly (because DNA polymerase
# moves in the reverse direction), thus it lives most of its life double-stranded.
# Forward (Lagging) Half Strand on the other hand, spends much more time being
# single-stranded, thus it's more prone to mutations.
# Cytosine (C) has a tendency to mutate into Thymine through Deamination, which we can
# use to find the ori.
# C occurs more often in one half of the genome (reverse strand) and less often in the
# other half (forward strand).
# Specifically, on the Forward (Lagging) half-strand the number of C is the highest,
# and on the Reverse (Leading) half-strand the number of C is the lowest. Thus, when
# sliding a window of size len(Genome) / 2 over the Genome, the part where the amount
# of C is increasing would represent the Forward (Lagging) half strand, and the part
# where the amount of C id decreasing represents the Reverse (Leading) half strand.
# This means that ter is at the point where the number of C is the highest (where
# forward strand transitions into the reverse strand); and Ori is at the point where
# the amount of C is the lowest.

# SymbolArray takes a string Genome and a character Symbol, and returns
# a symbol array that represents the count of Cytosine within the half window.
# The range where the count of C is decreasing will correspond to the Reverse Half
# Strand (Leading Half Strand), and the range where the count of C is increasing will
# correspond to the Forward Half Strand (Lagging).
# - Ori will thus be located at the position where the Reverse (Leading) Half Strand
# transitions into the Forward Half Strand (where the number of C is the smallest);
# - Ter is roughly located opposite of the ori, and is at the position where the the
# number of C is the greatest.

# _____________________________________________________________________________________
# Algorithm Explanation:
# Since the genome is circular, we will start by extending the passed in genome to
# create an "overlapping" by half of its length. For example, if genome = 'ATGCCC', the
# extended genome will be 'ATGCCCATG'.
# We then slide a window of size len(Genome) / 2 over the Genome and count the number
# of C within that window.

# For example,
# Input:
# Genome = "ATGCCC"
# symbol = "C"
# Output: {0: 0, 1: 1, 2: 2, 3: 3, 4: 2, 5: 1}


def SymbolArray(Genome, symbol):
    symbolArray = {}
    genomeLen = len(Genome)
    windowSize = genomeLen // 2
    extendedGenome = Genome + Genome[0:windowSize]
    symbolArray[0] = PatternCount(extendedGenome[0:windowSize], symbol)
    for i in range(1, genomeLen):
        symbolArray[i] = (
            symbolArray[i - 1]
            - (extendedGenome[i - 1] == symbol)
            + (extendedGenome[i + windowSize - 1] == symbol)
        )
    return symbolArray
