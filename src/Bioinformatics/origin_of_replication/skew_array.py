# Finding Origin of Replication through Deamination.
#
# Due to deamination the difference in amounts of C and G within a genome
# changes. Each Forward (Lagging) Half Strand has more G than C, and each Reverse
# (Leading) Half Strand has more C than G.
# The difference between the counts of G and C (#G - #C) is therefore positive on the
# Forward (Lagging) Half Strand and negative on the Reverse (Leading) Half Strand.
# Thus, on the Forward Half Strand #G - #C is increasing, and on the Reverse Half
# Strand #G - #C is decreasing. This means that the position at which #G - #C is
# the highest is the Ter point, and the position at which #G - #C is the lowest is
# the Ori.

# SkewArray function takes a string Genome and produces an array corresponding to
# the differences between the number of G and the number of C starting from index 1.
# Skew[1] corresponds to Genome[0]
# Minimum Skew (#G - #C) is the Ori location.

# For example,
# Input:
# Genome = 'CCAGTAGG'
# Result: [0, -1, -2, -2, -1, -1, -1, 0, 1]


def SkewArray(Genome):
    Skew = [0] * (len(Genome) + 1)
    for i in range(len(Genome)):
        char = Genome[i]
        if char == "A" or char == "T":
            Skew[i + 1] = Skew[i]
        elif char == "C":
            Skew[i + 1] = Skew[i] - 1
        elif char == "G":
            Skew[i + 1] = Skew[i] + 1
    return Skew


# MinimumSkew function uses SkewArray to find all the indexes where minimum skew
# (#G - #C) occurs.
# For example,
# Input:
# Genome = 'CCAGTAGG'
# Result: [2, 3]


def MinimumSkew(Genome):
    positions = []
    skewArray = SkewArray(Genome)
    minValue = min(skewArray)
    positions = [i for i in range(len(skewArray)) if skewArray[i] == minValue]
    return positions
