# HammingDistance is used to compute the number of differences between two
# k-mers P and Q. If the k-mers only differ in 1 nucleotide, then their
# Hamming Distance is 1, and so on. This function can be used to search a
# DNA string for potential DnaA boxes.

# For example,
# Input:
# p = "GTGTACCT"
# q = "GAGTAGCT"

# Output: 2


def HammingDistance(p, q):
    differences = 0
    if len(p) == len(q):
        for i in range(len(p)):
            if p[i] != q[i]:
                differences = differences + 1
    return differences
