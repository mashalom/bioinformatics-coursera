# PatternMatching function is used to find all occurences of a substring Pattern
# in a string Genome. It returns a list indexes of the first character where the
# matching patterns occur.

# For example,
# Input:
# Pattern = "GTT"
# Genome = "GTTGAGTTGTTAGT"
#
# Output: [0, 5, 8]


def PatternMatching(Pattern, Genome):
    positions = []
    for i in range(len(Genome) - len(Pattern) + 1):
        # fmt: off
        if Genome[i:i + len(Pattern)] == Pattern:
            positions.append(i)
        # fmt: on
    return positions
