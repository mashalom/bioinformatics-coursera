# FrequentWords function takes a DNA string Text and an integer k representing
# the length of the most common kmer to search for, and returns an array of all
# most frequently occurring k-mers in Text.
#
# For example:
# Input:
# Text = 'AACTGAACTG'
# k = 3
# Output: ['AAC', 'CTG']


def FrequentWords(text, k):
    words = []
    if len(text) >= k:
        freqDict = FrequencyMap(text, k)
        maxFrequency = max(freqDict.values())
        for sequence in freqDict:
            if freqDict[sequence] == maxFrequency:
                words.append(sequence)
    return words


# FrequencyMap function takes a DNA string Text and an integer k representing the
# length of the k-mers that will be used to construct a frequence map of each one of
# them. The output of this function is a dictionary/map containing kmer to frequency
# mappings.
#
# For example,
# Input:
# text = 'ACGTACGTG'
# k = 4
#
# Output: {
#   'ACGT': 2,
#   'CGTA': 1,
#   'GTAC': 1,
#   'TACG': 1,
#   'CGTG': 1
#  }


def FrequencyMap(text, k):
    freq = {}
    for i in range(len(text) - k + 1):
        pattern = text[i : i + k]
        if pattern in freq:
            freq[pattern] = freq[pattern] + 1
        else:
            freq[pattern] = 1

    return freq
