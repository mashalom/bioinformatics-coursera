# PatternCount function takes a DNA string Text and a k-mer string Pattern
# to be found in Text, and outputs how many times the k-mer occurs in the DNA string.

# For example:
# Input:
# Text = 'GCGCG'
# Pattern = 'GCG'
# Output: 2


def PatternCount(text, pattern):
    count = 0
    for i in range(len(text) - len(pattern) + 1):
        if text[i : i + len(pattern)] == pattern:
            count += 1
    return count
