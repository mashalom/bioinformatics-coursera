from bioinformatics.motif_search.count import CountWithPseudocounts

# Input: Array of strings representing motifs.
# Output: a dictionary of nucleotide to their profiles mappings.

# Input: ["AACGTA", "CCCGTT", "CACCTT"]
# Output:
# {'A': [0.33, 0.66, 0.0, 0.0, 0.0, 0.33],
#  'C': [0.66, 0.33, 1.0, 0.33, 0.0, 0.0],
#  'G': [0.0, 0.0, 0.0, 0.66, 0.0, 0.0],
#  'T': [0.0, 0.0, 0.0, 0.0, 1.0, 0.66]}


def Profile(Motifs):
    motifLen = len(Motifs[0])
    inputArrLen = len(Motifs)
    nucleotides = ["A", "C", "G", "T"]
    count = {}
    # initialize count dictionary
    for n in nucleotides:
        count[n] = [0] * motifLen
    # get count dictionary
    for i in range(len(Motifs)):
        for j in range(len(Motifs[i])):
            symbol = Motifs[i][j]
            count[symbol][j] += 1
    profiles = count
    for n in nucleotides:
        for i in range(len(count[n])):
            profiles[n][i] = profiles[n][i] / inputArrLen
    return profiles


# This function generates normalized profile for a motif matrix by eliminating
# probability = 0 for certain nucleotides.

# Input: An array of motif strings Motifs.
# Output: A dictionary containing nucleotides to their profile mappings.

# Sample Input: ["AACGTA", "CCCGTT", "CACCTT", "GGATTA", "TTCCGG"]
# Output: {
#          'A': [0.2222, 0.3333, 0.2222, 0.1111, 0.1111, 0.3333],
#          'C': [0.3333, 0.2222, 0.5555, 0.3333, 0.1111, 0.1111],
#          'G': [0.2222, 0.2222, 0.1111, 0.3333, 0.2222, 0.2222],
#          'T': [0.2222, 0.2222, 0.1111, 0.2222, 0.5555, 0.3333]
#         }


def ProfileWithPseudocounts(Motifs):
    k = len(Motifs[0])
    numOfMotifs = len(Motifs)
    normilizedTotal = numOfMotifs + 4
    countDict = CountWithPseudocounts(Motifs)
    profile = {"A": [1] * k, "C": [1] * k, "G": [1] * k, "T": [1] * k}
    for nucl in countDict:
        for i in range(len(countDict[nucl])):
            count = countDict[nucl][i]
            profile[nucl][i] = count / normilizedTotal
    return profile
