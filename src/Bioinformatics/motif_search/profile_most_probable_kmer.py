from bioinformatics.motif_search.probability import Pr

# ProfileMostProbableKmer returns a kmer of length "k" that is most likely to be the
# consensus motif based on a nucleotide sequence "text" and a profile matrix "profile".

# For example:
# Input:
# text: "ACCTGTTTATTGCCTAAGTTCCGAACAAACCCAATATAGCCCGAGGGCCT"
# k: 5
# profile: {
#           'A': [0.2, 0.2, 0.3, 0.2, 0.3],
#           'C': [0.4, 0.3, 0.1, 0.5, 0.1],
#           'G': [0.3, 0.3, 0.5, 0.2, 0.4],
#           'T': [0.1, 0.2, 0.1, 0.1, 0.2]
#          }

# Output: "CCGAG"


def ProfileMostProbableKmer(text, k, profile):
    mostProbableKmer = text[0:k]
    highestProbability = 0
    for i in range(len(text) - k + 1):
        kmer = text[i : i + k]
        probability = Pr(kmer, profile)
        if probability > highestProbability:
            highestProbability = probability
            mostProbableKmer = kmer
    return mostProbableKmer
