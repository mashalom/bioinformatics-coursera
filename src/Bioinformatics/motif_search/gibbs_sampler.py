import random
from bioinformatics.motif_search.motifs import RandomMotifs
from bioinformatics.motif_search.probability import Pr
from bioinformatics.motif_search.score import Score
from bioinformatics.motif_search.profile import ProfileWithPseudocounts


# This function takes a dictionary of nucleotides and their unscaled probabilities
# and normalizes them so they add up to 1.

# Sample Input:
# Probabilities: {'A': 0.1, 'C': 0.1, 'G': 0.1, 'T': 0.1}
# Output: {'A': 0.25, 'C': 0.25, 'G': 0.25, 'T': 0.25}
def Normalize(Probabilities):
    sumOfProbabilities = sum(Probabilities.values())
    normalizedProbablities = {}
    for key in Probabilities.keys():
        probability = Probabilities[key]
        normalizedProbablities[key] = probability / sumOfProbabilities
    return normalizedProbablities


# This function takes a dictionary of nucleotides and their weighted probabilities and
# and returns a random nucleotide letter depending on probabilities.

# For example, if A has the highest probability then most likely A will be returned.
# However, if all nucleotide letters have the same probability (0.25) then each one of
# them has an equal probability of being returned.

# Sample Input:
# Probabilities: {'A': 0.25, 'C': 0.25, 'G': 0.25, 'T': 0.25}
# Output: 'C' (Randomly selected)

# Probabilities: {'A': 1.0, 'C': 0.0, 'G': 0.0, 'T': 0.0}
# Output: 'A' (Result will always be A since all other probabilities are 0)
def WeightedDie(Probabilities):
    kmer = ""
    p = random.uniform(0, 1)
    acc = 0
    for nucl in Probabilities.keys():
        probability = Probabilities[nucl]
        if acc <= p < acc + probability:
            kmer = nucl
            return kmer
        acc += probability
    return kmer


# This function returns a randomly selected kmer in a string Text given provided
# profile taking into consideration probabilities of each k-mer in the string Text.

# For example, if based on the profile AG is the most probable string in Text AATCAGCC
# then there is a highers chance that the function will return it. But it might also
# return other kmers.

# Sample input:
# Text: "AAACCCAAACCC"
# profile: {'A': [0.5, 0.1], 'C': [0.3, 0.2], 'G': [0.2, 0.4], 'T': [0.0, 0.3]}
# k: 2

# Result: "AG" (Other kmers can also be returned)
def ProfileGeneratedString(Text, profile, k):
    probabilities = {}
    for i in range(len(Text) - k + 1):
        kmer = Text[i : i + k]
        probabilities[Text[i : i + k]] = Pr(kmer, profile)
    probabilities = Normalize(probabilities)
    result = WeightedDie(probabilities)
    return result


# GibbsSampler Algorithm is a more efficient algorithm compared to the Randomized
# Search Algorithm. It is more cautious in the way that it picks 1 k-mer from the
# current set of motifs at each iteration, and decides to either keep it or replace it
# with a new one. Thus, it only updates 1 motif in the DNA list at a time, as opposed
# to Randomized Search which can drastically change "selected" k-mers at every
# iteration.

# For example:
#
# Randomized Search   (k = 3)
# -------------------------------------
# ttaccttAAC                 tTACcttaac
# gATAtctgtc                 gatATCtgtc
# ACGgcgttcg     ----->      acggcgTTCg
# ccctAAAgag   next iter.    ccctaaAGAg
# cgtcAGAggt                 CGTcagaggt


# Gibbs Sampler       (k = 3)
# -------------------------------------
# ttaccttAAC                 ttaccttAAC
# gATAtctgtc                 gatatcTGTc
# ACGgcgttcg     ----->      ACGgcgttcg
# ccctAAAgag   next iter.    ccctAAAgag
# cgtcAGAggt                 cgtcAGAggt

# Note that Randomized Search algorithm has a potential to completely change selected
# k-mers in every DNA string. Whereas, Gibbs Search algorithm only changes one DNA
# string at a time.

# _____________________________________________________________________________________
# Gibbs Sampler Description:
# First, randomly select motifs of length k in every DNA string.
# DNA = [
#   'ttacctTAAC',
#   'gatGTCTgtc',
#   'CCGGcgttag',
#   'cACTAacgag',
#   'cgtcagAGGT'
# ]
# Randomly selected motifs are = ['TAAC', 'GTCT', 'CCGG', 'ACTA', 'AGGT']
# At each iteration:
# 1. Randomly select one of the DNA strings (from 0 to len(Dna)) to "delete/ignore"
#    when forming a profile using the randomly selected motifs.
#
#    Assuming randomly selected DNA string to "ignore" is (i=2) DNA[2], then the Motifs
#    are: ['TAAC', 'GTCT', 'ACTA', 'AGGT']. 'CCGG' is "ignored".
# 2. Using Laplace's Rule of Succession create Profile for the Motifs.
#    Profile:
#     A: [3/8, 2/8, 2/8, 2/8],
#     C: [1/8, 2/8, 2/8, 2/8],
#     G: [2/8, 2/8, 2/8, 2/8],
#     T: [2/8, 2/8, 2/8, 3/8]
# 3. Randomly select a motif from the "ignored" DNA string (CCGGcgttag), taking into
#    account weighted probability of each kmer in the string (given the current
#    profile).
#    Assuming the selected kmer is: "GCGT".
#    Kmers with highest probability have a higher chance of being randomly selected,
#    which leads to creating a more "biased" profile on subsequent iterations, which
#    in turn points toward the correct motifs.
# 4. Add the newly selected motif back to the list:
#    ['TAAC', 'GTCT', 'ACTA', 'AGGT', 'GCGT'], and calculate the Score of this new
#    Motifs list as well as the original Motifs list:
#    ['TAAC', 'GTCT', 'CCGG', 'ACTA', 'AGGT'], and select the Motifs that have best
#    Score.
#
# Continue to iterating N times until the Score can no longer be improved.

# Input:
# Dna: list of DNA strings
# k: length of k-mer motifs to look for
# N: number of iterations to run the algorithm
def GibbsSampler(Dna, k, N):
    # randomly select motifs in each Dna string
    BestMotifs = RandomMotifs(Dna, k)
    for i in range(N):
        # randomly select one of the DNA motifs and create Profile excluding that motif
        randomIndex = random.randint(0, len(Dna) - 1)
        tempMotifs = list(BestMotifs)
        del tempMotifs[randomIndex]
        # construct weighted profile matrix from all motifs except the one that was
        # excluded
        weightedProfile = ProfileWithPseudocounts(tempMotifs)
        # randomly select a motif from the "deleted" Dna string taking into account
        # weighted probability of each kmer in that Dna string
        motif = ProfileGeneratedString(Dna[randomIndex], weightedProfile, k)
        tempMotifs.append(motif)
        # update best motifs list if new score is better than the old
        if Score(tempMotifs) < Score(BestMotifs):
            # Update the motif in the BestMotifs list
            BestMotifs[randomIndex] = motif
    return BestMotifs
