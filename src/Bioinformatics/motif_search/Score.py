from bioinformatics.motif_search.count import Count
from bioinformatics.motif_search.consensus import Consensus

# Score function accepts a list of Motifs and returns their score
# based on how far they are from their consensus motif.
# The lower the score the better (it means that the motifs are most
# similar to each other).

# Input:  A set of k-mers Motifs
# Motifs = ["ACGT", "AGGT", "ACGT"]
# Output: The score of these k-mers.
# Returns: Score of 1


def Score(Motifs):
    score = 0
    countDict = Count(Motifs)
    consensus = Consensus(Motifs)
    for l in range(len(consensus)):
        currentNucl = consensus[l]
        score += len(Motifs) - countDict[currentNucl][l]
    return score
