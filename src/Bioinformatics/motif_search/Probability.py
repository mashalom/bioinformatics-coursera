# This function calculates probability of a certain string being a consensus motif
# k-mer. The higher the probability, the closer the string to the consensus string of a
# DNA profile

# Input:  String Text and profile matrix Profile
# Output: Probability of Text being the consensus given its Profile matrix.

# Input example:
# Text: "ACGGGGATTACC"
# Profile: {
#           'A': [0.2, 0.2, 0.0, 0.0, 0.0, 0.0, 0.9, 0.1, 0.1, 0.1, 0.3, 0.0],
#           'C': [0.1, 0.6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4, 0.1, 0.2, 0.4, 0.6],
#           'G': [0.0, 0.0, 1.0, 1.0, 0.9, 0.9, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0],
#           'T': [0.7, 0.2, 0.0, 0.0, 0.1, 0.1, 0.0, 0.5, 0.8, 0.7, 0.3, 0.4]
#          }

# Output example: 0.0008398080000000002


def Pr(Text, Profile):
    acc = 1
    for i in range(len(Text)):
        nucleotide = Text[i]
        acc = acc * Profile[nucleotide][i]
    return round(acc, 4)
