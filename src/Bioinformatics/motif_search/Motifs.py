import random
from bioinformatics.motif_search.profile_most_probable_kmer import (
    ProfileMostProbableKmer,
)


# This function produces a list of randomly selected k-mers.
# Input: Dna: array of strings representing DNA sequences.
# k: integer representing the length of motifs to be randomly selected.

# Sample Input:
# Dna: ["TTACCTTAAC", "GATGTCTGTC", "ACGGCGTTAG", "CCCTAACGAG", "CGTCAGAGGT"]
# k: 3
#
# Output: ["TTA", "TGT", "TAG", "CCC", "GAG"]


def RandomMotifs(Dna, k):
    randomMotifs = []
    for string in Dna:
        randomStartingIndex = random.randint(0, len(string) - k)
        randomMotifs.append(string[randomStartingIndex : randomStartingIndex + k])
    return randomMotifs


# Motifs function returns an array of most probable motifs given a list of Dna strings
# and profile.

# Sample Input:
# Profile: {
#           'A': [0.8, 0.0, 0.0, 0.2],
#           'C': [0.0, 0.6, 0.2, 0.0],
#           'G': [0.2, 0.2, 0.8, 0.0],
#           'T': [0.0, 0.2, 0.0, 0.8]
#          }
# Dna: ["TTACCTTAAC", "GATGTCTGTC", "ACGGCGTTAG", "CCCTAACGAG", "CGTCAGAGGT"]
#
# Sample Output: ["ACCT", "ATGT", "GCGT", "ACGA", "AGGT"]


def Motifs(Profile, Dna):
    bestMotifsArray = []
    k = len(Profile["A"])
    for kmer in Dna:
        bestMotifsArray.append(ProfileMostProbableKmer(kmer, k, Profile))
    return bestMotifsArray
