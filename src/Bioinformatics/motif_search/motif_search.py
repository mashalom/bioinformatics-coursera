from bioinformatics.motif_search.score import Score
from bioinformatics.motif_search.profile import Profile
from bioinformatics.motif_search.profile_most_probable_kmer import (
    ProfileMostProbableKmer,
)


# Note on Greedy Algorithms:
# Greedy Algorithms aim to find the "closest" possible solution on each iteration.
# They do not guarantee to always produce the correct/best result, instead they find
# an approximation to the correct/besr result.
# However, unlike Brute Force Algorithms they are fast and not always as accurate.


# Greedy algorithm for basic motif search.

# This motif search algorithm outputs an array containing motif k-mers that most closely
# match each other given an array of DNA strings that represent the upstream regions of
# genes where transcription factor binding site can be found.

# Note: This algorithm is fast but does not provide the most accurate results.

# Input:
# Dna - array of strings representing the genes upstream regions, where we will be
#       looking for the k-mers that most closely match each other.
# k   - integer representing the length of the k-mers to look for in the Dna strings.
# t   - length of the Dna array (not really used)

# Example:
# Input:
# Dna = ["GGCGTT", "AAGAAT", "CACGTC"]
# k = 3

# Output: ['GCG', 'AAG', 'ACG']


def MotifSearch(Dna, k):
    bestMotifList = [kmer[0:k] for kmer in Dna]
    bestScore = Score(bestMotifList)
    currentMotifList = []
    for i in range(len(Dna[0]) - k + 1):
        kmerToCompare = Dna[0][i : i + k]
        currentMotifList.append(kmerToCompare)
        profile = Profile(currentMotifList)
        for dnaStr in Dna[1 : len(Dna)]:
            bestMotifMatch = ProfileMostProbableKmer(dnaStr, k, profile)
            currentMotifList.append(bestMotifMatch)
            profile = Profile(currentMotifList)
        currentScore = Score(currentMotifList)
        if bestScore > currentScore:
            bestScore = currentScore
            bestMotifList = currentMotifList
        # clear profile and currentMotifList for next iteration with next kmer
        profile = []
        currentMotifList = []
    return bestMotifList
