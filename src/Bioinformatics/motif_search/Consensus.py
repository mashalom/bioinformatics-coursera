from bioinformatics.motif_search.count import Count

# Input:  A set of kmers Motifs, provided as a list of strings.
# For example: ["AACGTA", "CCCGTT", "CACCTT"]
# Output: A consensus string of Motifs.
# Sample output: "CACGTT"

# This function iterates over a count matrix and constructs a string of
# most frequently occurring nucleotides at each position.


def Consensus(Motifs):
    consensus = ""
    k = len(Motifs[0])
    count = Count(Motifs)
    for j in range(k):
        m = 0
        frequentSymbol = ""
        for symbol in "ACGT":
            if count[symbol][j] > m:
                m = count[symbol][j]
                frequentSymbol = symbol
        consensus += frequentSymbol
    return consensus
