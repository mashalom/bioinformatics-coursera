from bioinformatics.motif_search.profile_most_probable_kmer import (
    ProfileMostProbableKmer,
)
from bioinformatics.motif_search.profile import ProfileWithPseudocounts
from bioinformatics.motif_search.score import Score

# Note on Greedy Algorithms:
# Greedy Algorithms aim to find the "closest" possible solution on each iteration.
# They do not guarantee to always produce the correct result, but they do find
# an approximation to the correct result.
# However, unlike Brute Force Algorithms they are fast, but not as accurate.


# Greedy algorithm for motif search using weighted profile matrix, i.e. using Laplace's
# Rule of Succession to avoid eliminating potential k-mers that have high probability
# score but have few nucleotides with probability 0.
# Laplace's Rule of Succession states that there will never be certain probability of 0
# or 1, unless we are talking about facts. We should always give a small probability to
# events that are unlinkely to happen.
# Since our sample size of DNA strings is usually relatively small, we should give a
# small probability to nucleotides that do not appear in a certain location of a
# string.

# For example, given Count, and Profile:
# ___________________________________________
# {'A': [0, 1, 1]}      {'A': [0.0, 0.2, 0.2]}
# {'C': [2, 3, 1]}      {'C': [0.4, 0.6, 0.2]}
# {'G': [2, 0, 0]}      {'G': [0.4, 0.0, 0.0]}
# {'T': [1, 1, 3]}      {'T': [0.2, 0.2, 0.6]}

# kmers like "ACC" would get ignored even though that kmer has a high probability. A
# lot of similar kmers that could be potential motifs will get eliminated this way.

# That's why for this algorithm we will apply Laplace's Rule of Succession to make sure
# kmers like the one above do not get ignored.

# After Laplace's Rule, the Count and Profile become:
# ___________________________________________
# {'A': [1, 2, 2]}      {'A': [1/9, 2/9, 2/9]}
# {'C': [3, 4, 2]}      {'C': [3/9, 4/9, 2/9]}
# {'G': [3, 1, 1]}      {'G': [3/9, 1/9, 1/9]}
# {'T': [2, 2, 4]}      {'T': [2/9, 2/9, 4/9]}

# Note: New weighted sum becomes 5 + 4 = 9

# _____________________________________________________________________________________
# Algorithm Description:
#
# Goal: Given an array of DNA strings, Dna, that represent the upstream regions of
# genes where transciption factor binding site can be found, we want to find the k-mers
# in those DNA strings that most closely match each other. Those k-mers will be the
# Regulatory Motif.
#
# 1. Outer loop: run through each possible k-mer in the first DNA string (Dna[0])
# 2. At each iteration of the outer loop, identify the best matches for the current
#    k-mer in all thr following DNA strings (Dna[1:n]), thus creating a set of motifs
#    at each step.
# 3. Score and compare each set of motifs at each iteration (previous vs current)
#    and return the best scoring list of Motifs.


# Input:
# Dna - list of DNA strings representing the upstream regions of genes to be searched
#       for transription factor binding sites. Dna = ['AACGC', 'ACCGT', 'CCGGA']
# k   - length of motif to look for. k = 3

# Output: ['ACG', 'ACC', 'CCG']


def GreedyMotifSearchWithPseudocounts(Dna, k):
    BestMotifs = [kmer[0:k] for kmer in Dna]
    currentMotifs = []
    bestScore = Score(BestMotifs)
    # iterate over each k-mer in the first string in the Dna array
    for i in range(len(Dna[0]) - k + 1):
        kmerToCompare = Dna[0][i : i + k]
        currentMotifs.append(kmerToCompare)
        profile = ProfileWithPseudocounts(currentMotifs)
        # iterate over each subsequent Dna string (starting at Dna[1])
        # to find the best k-mer match for "kmerToCompare", updating
        # "profile" on every iteration with the best kmer match found
        for dnaStr in Dna[1 : len(Dna)]:
            bestMotifMatch = ProfileMostProbableKmer(dnaStr, k, profile)
            currentMotifs.append(bestMotifMatch)
            profile = ProfileWithPseudocounts(currentMotifs)
        currentScore = Score(currentMotifs)
        # lower score means better match, so update bestMotifs list if
        # current score is lower than the best score
        if bestScore > currentScore:
            bestScore = currentScore
            BestMotifs = currentMotifs
        profile = []
        currentMotifs = []
    return BestMotifs
