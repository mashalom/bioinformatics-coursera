# This function produces a dictionary containing counts for each nucleotide.
# Input:  Motifs: A list of strings representing k-mers
# Output: Returns counts metrix that is a dictionary with maps of nucletiode to its
# count per position.
# Sample output: { 'A': [0, 1, 2, 1, 0], 'C': [4, 1, 0, 9, 0].. }


def Count(Motifs):
    motifLen = len(Motifs[0])
    nucleotides = ["A", "C", "G", "T"]
    count = {}
    # initialize count dictionary
    for n in nucleotides:
        count[n] = [0] * motifLen
    for i in range(len(Motifs)):
        for j in range(len(Motifs[i])):
            symbol = Motifs[i][j]
            count[symbol][j] += 1
    return count


# This function produces a dictionary containing counts for each nucleotide using
# Laplace's Rule of Succession (Adding +1 to each count to avoid dismissing the motifs
# that have a nucloetide with probability of 0).

# Input: List of strings Motifs, representing DNA motifs.
# Output: Dictionary of nucleotides

# Sample Input: ["AACGTA", "CCCGTT", "CACCTT", "GGATTA", "TTCCGG"]
# Sample Output: {
#                 'A': [2, 3, 2, 1, 1, 3],
#                 'C': [3, 2, 5, 3, 1, 1],
#                 'G': [2, 2, 1, 3, 2, 2],
#                 'T': [2, 2, 1, 2, 5, 3]
#                }


def CountWithPseudocounts(Motifs):
    k = len(Motifs[0])
    MotifsDict = {"A": [1] * k, "C": [1] * k, "G": [1] * k, "T": [1] * k}
    for string in Motifs:
        for i in range(len(string)):
            nucleotide = string[i]
            MotifsDict[nucleotide][i] += 1
    return MotifsDict
