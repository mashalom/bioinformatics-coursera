from bioinformatics.motif_search.motifs import RandomMotifs, Motifs
from bioinformatics.motif_search.profile import ProfileWithPseudocounts
from bioinformatics.motif_search.score import Score

# Note on Randomized Algorithms:
# Randomized algorithms are good at finding approximate solutions fast but are not
# always guaranteed to be exact.
# There are however, 2 different types of Randomized Algorithms:
# 1. Las Vegas   - algorithms provide solutions that are guaranteed to be exact
#                  regradless of the fact that they rely on random selections.
# 2. Monte Carlo - algorithms are not guaranteed to return exact solutions, but they
#                  are able to quickly find a close approximation to the solution.

# RandomizedMotifSearch is a Monte Carlo algorithm. It does not always provide the most
# accurate result but it quickly finds an approximation to it.

# Randomized Motif Search finds best motifs in the given DNA array using randomized
# algorithm.
# This algorithm performs well when randomly selected k-mers do NOT produce a uniformly
# distributed Profile. When Profile contains the actual Motifs, it becomes "biased"
# towards the correct Motif. Therefore, if the randomly selected Motifs produce a
# uniform Profile, this algorithm won't be able to point us towards the direction of
# the actual motifs until later iterations when it is able to randomly pick one of the
# actual motifs.


# _____________________________________________________________________________________
# Algorithm Description:
#
# We want to find the motifs of length k that are most similar to each other among
# given DNA strings.
#
# Dna = [
#   'TTACCTTAAC',
#   'GATGTCTGTC',
#   'CCGGCGTTAG',
#   'CACTAACGAG',
#   'CGTCAGAGGT'
# ]
#
# 1. Begin from a collection of randomly selected k-mers Motifs in DNA to be the "Best
#    Motifs".
#    For example randomly selected Motifs are: ['TAAC', 'GTCT', 'CCGG', 'ACTA', 'AGGT']
#
# 2. Compute their Profile: Profile(Motifs).
# {'A': [0.4, 0.2, 0.2, 0.2]}
# {'C': [0.2, 0.4, 0.2, 0.2]}
# {'G': [0.2, 0.2, 0.4, 0.2]}
# {'T': [0.2, 0.2, 0.2, 0.4]}
#
# 3. Using that Profile, we can compute probability of every k-mer in all DNA strings.
#    For example, Probability('TTAC', Profile) = 0.2 x 0.2 x 0.2 x 0.2 = 0.0016, etc.
# The k-mers with highest probability will then be picked.
#
# Probabilities:
# ___________________
#  ttac    tacc    ACCT    cctt    ctta    ttaa    taac
# 0.0016  0.0016  0.0128  0.0064  0.0016  0.0016  0.0016
# .....
# Resulting Motifs with highest probability list = [ACCT, ATGT, GCGT, ACGA, AGGT]
#
# 4. Once we've constructed the new motifs, compare the score of current motifs with
#    the previous best motifs and update the best motifs list accordingly.
#
# 5. Continue iterating until the motifs list score stops improving

# Sample Input:
# Dna: ["AACGTA", "CCGATT", "GGACTA"]
# k: 3 (length of the most similar k-mer to search for.

# Output: ['ACG', 'CCG', 'ACT']


def RandomizedMotifSearch(Dna, k):
    CurrentMotifs = RandomMotifs(Dna, k)
    BestMotifs = CurrentMotifs
    count = 0
    while count <= 100:
        Profile = ProfileWithPseudocounts(CurrentMotifs)
        CurrentMotifs = Motifs(Profile, Dna)
        if Score(CurrentMotifs) < Score(BestMotifs):
            BestMotifs = CurrentMotifs
        else:
            count += 1
        CurrentMotifs = RandomMotifs(Dna, k)
    return BestMotifs
