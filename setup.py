import setuptools

setuptools.setup(
    name="bioinformatics",
    version="0.0.1",
    author="Maria Lomidze",
    author_email="marialomidze8@gmail.com",
    description="Bioinformatics module",
    # url="https://github.com/pypa/sampleproject",
    package_dir={'': 'src'},
    packages=setuptools.find_namespace_packages(where='src'),
    python_requires='>=3.6',
)